﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearDrag : MonoBehaviour {

	Rigidbody rigid;
	Vector3 gForce, linDrag;
	public float c;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		gForce = rigid.mass * (-9.81f) * Vector3.up;
		linDrag = -rigid.velocity * c;
		rigid.AddForce (gForce);
		rigid.AddForce (linDrag);
	}
}
