﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSteamVR : MonoBehaviour {

    public Transform head;
   public  Vector3 offset = new Vector3(0,-2,0);
    public float minForce = 3;

  public   Rigidbody rigid;
	// Use this for initialization
	void Start () {
  //      rigid = GetComponent<Rigidbody>();
        rigid.isKinematic = true;
        StartCoroutine(DelayedStart());
	}
	IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(3);
        rigid.isKinematic = false;
    }

	// Update is called once per frame
	void FixedUpdate () {
        transform.rotation = head.rotation;
        //  transform.LookAt(head);
        if (head != null) transform.position = head.position + offset;
    //    if (rigid.velocity.magnitude < minForce) rigid.velocity = Vector3.zero;
     //   Debug.DrawLine(transform.position, transform.position + rigid.velocity, Color.green);
	}
}
