﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebAttacher : MonoBehaviour
{
	public Web web;
	public WebSprings webSprings;
	public bool decaying;
	public	FixedJoint stickJoint;
	Rigidbody rigid;
	// Use this for initialization
	void Start ()
	{
		decaying = false;
		rigid = GetComponent <Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (rigid.velocity.magnitude > 500) {
			//	Debug.Log ("Velocity: " + rigid.velocity.magnitude);
			//	Debug.Break ();
			rigid.velocity = rigid.velocity.normalized * 400;
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (web != null && col.gameObject.name != "Web")
			web.Stick ();
		
	}

	void OnCollisionEnter (Collision col)
	{
		if (webSprings != null && decaying == false && (col.gameObject.name != "WebSprings" || col.gameObject.name != "WebSprings(Clone)") && stickJoint == null && col.gameObject.name != "[CameraRig]") {
			//Debug.Log ("Sticking to " + col.gameObject.name);
			//
			//if (col.gameObject.layer == 9) {
			if (true) {
				if (col.gameObject.GetComponent <Rigidbody> () == false) {
					Rigidbody tempRigid =	(Rigidbody)col.gameObject.AddComponent (typeof(Rigidbody));
					tempRigid.isKinematic = true;
				}
				col.gameObject.AddComponent (typeof(FixedJoint));
				FixedJoint[] tempJoints = col.gameObject.GetComponents<FixedJoint> ();
				stickJoint = tempJoints [tempJoints.Length - 1];
				stickJoint.connectedBody = gameObject.GetComponent <Rigidbody> ();
				GetComponent<Rigidbody> ().velocity = col.gameObject.GetComponent <Rigidbody> ().velocity;
				if (webSprings.sticking == false) {
					webSprings.sticking = true;
					//Debug.Log ("Just before holding");
					if (webSprings.holding) {
						//webSprings.webParticles [webSprings.webParticles.Count - 1].transform.position = webSprings.transform.position;
						//	webSprings.handJoint = (FixedJoint)webSprings.webParticles [webSprings.webParticles.Count - 1].gameObject.AddComponent (typeof(FixedJoint));
						//	webSprings.handJoint.connectedBody = webSprings.webShooter.GetComponent<Rigidbody> ();
						SpringJoint tempSpring =	(SpringJoint)webSprings.webParticles [webSprings.webParticles.Count - 1].gameObject.AddComponent (typeof(SpringJoint));
						tempSpring.connectedBody = webSprings.webParticles [webSprings.webParticles.Count - 2];
						Debug.Log ("Trying to tie to hand!");


					}
				}
				/*	SpringJoint[] allSprings = GetComponents <SpringJoint> ();
				foreach (SpringJoint joint in allSprings) {
					Destroy (joint);
				}*/
				GetComponent <SphereCollider> ().enabled = false;
				//Physics.IgnoreCollision (GetComponent <SphereCollider> (), col.collider);
				/*GetComponent <SpringJoint> ().tolerance = 100;
				GetComponent <SpringJoint> ().minDistance = 0;
				GetComponent <SpringJoint> ().maxDistance = 100;*/
			} else if (col.gameObject.layer != 8) {
				webSprings.Stick ();
				GetComponent <Rigidbody> ().isKinematic = true;
				Debug.Log ("Kinematic because : " + col.gameObject.name);
			} 
		}

	}
}
