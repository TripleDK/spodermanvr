﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetControllerIndex : MonoBehaviour {

    public TextMesh hint;
    int progress;
   public SteamVR_TrackedObject leftController, rightController;

	// Use this for initialization
	void Start () {
        // hint.text = "Press trigger on left controller!";
        hint.text = "";
        hint.transform.SetParent(Camera.main.transform);
        hint.transform.localPosition = new Vector3(0, 0, 8);
        hint.transform.LookAt(Camera.main.transform);
        hint.transform.Rotate(hint.transform.up, 180);
        progress = 0;
    }
	
	// Update is called once per frame
	void Update () {
		if(progress == 0 && Input.GetKeyDown("joystick button 14"))
        {
            WebShooterSprings.leftContrIndex= (int)leftController.index;
            //  hint.text = "Press trigger on right controller!";
            hint.text = "";
            progress = 1;
        }
        if(progress == 1 && Input.GetKeyDown("joystick button 15"))
        {
            WebShooterSprings.rightContrIndex = (int)rightController.index;
            Debug.Log("right ind: " + WebShooterSprings.rightContrIndex);
            hint.text = "";
            progress = 2;
        }
	}
}
