﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebShooterSprings : MonoBehaviour
{

	public GameObject webSprings, webSpringsParticle;

	public bool holding;
	public WebSprings currentWeb;
	public Animator shooterAnim;
	public AudioClip webShoot;
	Rigidbody rigid;
	public Rigidbody playerRigid;
	public  int controllerIndex;
	static public int leftContrIndex, rightContrIndex;

	// Use this for initialization
	void Start ()
	{
		rigid = GetComponent <Rigidbody> ();
		holding = false;
		controllerIndex = (int)transform.parent.gameObject.GetComponent<SteamVR_TrackedObject> ().index;
		Physics.IgnoreLayerCollision (8, 8, true);
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (Input.GetKeyDown (KeyCode.Space) || (Input.GetKeyDown ("joystick button 15") && controllerIndex == rightContrIndex) || (Input.GetKeyDown ("joystick button 14") && controllerIndex == leftContrIndex)) {
			holding = true;
			currentWeb = ((GameObject)Instantiate (webSprings, transform.position, transform.rotation)).GetComponent <WebSprings> ();
			currentWeb.webShooter = this.gameObject;
			currentWeb.holding = true;
			currentWeb.DelayedStart ();
			shooterAnim.SetTrigger ("shoot");
			AudioSource.PlayClipAtPoint (webShoot, transform.position);
		}
		//Holding == true is only for debugging!!!
		if (holding == true || Input.GetKey (KeyCode.Space) || (Input.GetKey ("joystick button 15") && controllerIndex == rightContrIndex) || (Input.GetKey ("joystick button 14") && controllerIndex == leftContrIndex)) {
			if (currentWeb.sticking == false) {
				//	if ((currentWeb.webParticles [currentWeb.webParticles.Count - 1].transform.position - transform.position).magnitude >= currentWeb.stringLength)
				currentWeb.AddParticle ();
			} else {
				//playerRigid.AddForce (currentWeb.webParticles [currentWeb.webParticles.Count - 1].GetComponent<Rigidbody> ().velocity, ForceMode.Force);
				Vector3 forceDir = (currentWeb.webParticles [currentWeb.webParticles.Count - 2].transform.position - playerRigid.transform.position);
				Vector3 force = forceDir.normalized * (forceDir.magnitude - currentWeb.stringLength) * currentWeb.springConstant;
				playerRigid.AddForce (force);
				Debug.DrawLine (transform.position, transform.position + force, Color.green);

				//	currentWeb.webParticles [currentWeb.webParticles.Count - 1].velocity = Vector3.zero;
			}
			if (currentWeb != null) {
				if (currentWeb.webParticles.Count > 1) {
					//	currentWeb.webParticles [currentWeb.webParticles.Count - 1].transform.position = transform.position;
					//	currentWeb.webParticles [currentWeb.webParticles.Count - 1].transform.rotation = transform.rotation;
				}
			}
				

		}

		if (Input.GetKeyUp (KeyCode.Space)|| (Input.GetKeyUp ("joystick button 15") && controllerIndex == rightContrIndex) || (Input.GetKeyUp ("joystick button 14") && controllerIndex == leftContrIndex)) {

			Debug.Log ("Let it go!!");
			holding = false;
			currentWeb.holding = false;
			if (currentWeb.handJoint)
				Destroy (currentWeb.handJoint);
			currentWeb.WebDecay ();
			currentWeb = null;
			shooterAnim.SetTrigger ("release");
		}
	}

	void FixedUpdate ()
	{

	}
}
