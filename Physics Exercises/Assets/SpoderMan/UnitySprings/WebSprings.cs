﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebSprings : MonoBehaviour
{

	public float damper = 0.2f;
	public bool bendSprings;
	public bool sticking, holding;
	public GameObject webSpringParticle;
	LineRenderer rend;
	public float webExplode;
	public float springConstant, stringLength, particleMass;
	public List<Rigidbody> webParticles = new List<Rigidbody> ();
	public GameObject webShooter;
	public FixedJoint handJoint;

	// Use this for initialization
	void Awake ()
	{
		rend = GetComponent <LineRenderer> ();
		rend.numPositions = 1;
		rend.SetPosition (0, transform.position);

		//Setting first particle
		Rigidbody tempParticle = ((GameObject)(Instantiate (webSpringParticle, transform.position, transform.rotation))).GetComponent <Rigidbody> ();
		tempParticle.gameObject.GetComponent <WebAttacher> ().webSprings = this;
		tempParticle.gameObject.name = "WebParticle 1";
		webParticles.Add (tempParticle);
		tempParticle.transform.parent = transform;
		tempParticle.mass = particleMass;
	}

	void Start ()
	{
		
		/*Rigidbody tempParticle = ((GameObject)(Instantiate (webSpringParticle, transform.position, transform.rotation))).GetComponent <Rigidbody> ();
		tempParticle.gameObject.GetComponent <WebAttacher> ().webSprings = this;
	
		webParticles.Add (tempParticle);

		tempParticle.transform.parent = transform;
		tempParticle.mass = particleMass;
		SpringJoint tempSpring = tempParticle.gameObject.GetComponent <SpringJoint> ();
		Destroy (tempSpring); */
		//		AddParticle ();
	}

	public void DelayedStart ()
	{
		handJoint = (FixedJoint)webParticles [0].gameObject.AddComponent (typeof(FixedJoint));
		handJoint.connectedBody = webShooter.GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update ()
	{
		rend.numPositions = webParticles.Count + 1;
		for (int i = 0; i < webParticles.Count; i++) {
			rend.SetPosition (i, webParticles [i].transform.position);
		}
		if (holding)
			rend.SetPosition (webParticles.Count, webShooter.transform.position);
		else
			rend.SetPosition (webParticles.Count, webParticles [webParticles.Count - 1].transform.position);
	}

	public void FixedUpdate ()
	{
		if (holding && !sticking) {
			AddParticle ();
		}
	}

	public void AddParticle ()
	{
		if (webParticles.Count > 1) {
			//	Debug.Log ((webParticles [webParticles.Count - 2].transform.position - transform.position).magnitude + ", is it further than: " + stringLength + "?");
			if ((webParticles [webParticles.Count - 2].transform.position - transform.position).magnitude >= stringLength) {
				Destroy (handJoint);
				webParticles [webParticles.Count - 1].AddForce (webShooter.transform.up * webExplode, ForceMode.Impulse);


				int particlesToAdd;

				particlesToAdd = (int)((webParticles [webParticles.Count - 1].position - webShooter.transform.position).magnitude / stringLength);
				//		Debug.Log ("Distance is: " + (webParticles [webParticles.Count - 1].position - webShooter.transform.position).magnitude + "; so we add " + particlesToAdd);


				//DEBUG
				//particlesToAdd = 1;

				/*for (int i = particlesToAdd; i > 0; i--) {
			Vector3 particlePosition;
			if (webParticles.Count > 0) {
				particlePosition = webShooter.transform.position + (webParticles [webParticles.Count - 1].position - webShooter.transform.position) * stringLength * (i) / particlesToAdd;
			} else {
				particlePosition = webShooter.transform.position;
			}*/
				Rigidbody tempParticle = ((GameObject)(Instantiate (webSpringParticle, webShooter.transform.position, transform.rotation))).GetComponent <Rigidbody> ();
				tempParticle.transform.SetParent (transform);
				tempParticle.gameObject.GetComponent <WebAttacher> ().webSprings = this;
				webParticles.Add (tempParticle);
				tempParticle.mass = particleMass;
				tempParticle.gameObject.name = "Webparticle " + (webParticles.Count);
				tempParticle.AddForce (webShooter.transform.up * webExplode, ForceMode.Impulse);
				if (webParticles.Count == 2) {
					webParticles [0].gameObject.AddComponent (typeof(SpringJoint));
					AddSpringValues (webParticles [0].gameObject.GetComponent <SpringJoint> ());
				}
				SpringJoint tempSpring = (SpringJoint)webParticles [webParticles.Count - 2].gameObject.AddComponent (typeof(SpringJoint));
				tempSpring.connectedBody = webParticles [webParticles.Count - 1];
				//	Debug.Log (tempSpring.gameObject.name + " connects with " + webParticles [webParticles.Count - 2].gameObject.name);
				AddSpringValues (tempSpring);
				if (bendSprings && webParticles.Count > 2) {

					tempSpring = (SpringJoint)webParticles [webParticles.Count - 3].gameObject.AddComponent (typeof(SpringJoint));
					tempSpring.connectedBody = webParticles [webParticles.Count - 1];
					//		Debug.Log (tempSpring.gameObject.name + " bend connects with " + webParticles [webParticles.Count - 1].gameObject.name);
					AddBendSpringValues (tempSpring);
				}

				handJoint = (FixedJoint)webParticles [webParticles.Count - 1].gameObject.AddComponent (typeof(FixedJoint));
				handJoint.connectedBody = webShooter.GetComponent<WebShooterSprings> ().GetComponent<Rigidbody> ();
			}
		} else { //There will always be one particle (See awake())
			Destroy (handJoint);
			webParticles [0].AddForce (webShooter.transform.up * webExplode, ForceMode.Impulse);
			Rigidbody tempParticle = ((GameObject)(Instantiate (webSpringParticle, transform.position, transform.rotation))).GetComponent <Rigidbody> ();
			tempParticle.gameObject.GetComponent <WebAttacher> ().webSprings = this;

			webParticles.Add (tempParticle);
			tempParticle.transform.parent = transform;
			tempParticle.mass = particleMass;
			tempParticle.gameObject.name = "WebParticle 2";
			handJoint = (FixedJoint)webParticles [webParticles.Count - 1].gameObject.AddComponent (typeof(FixedJoint));
			handJoint.connectedBody = webShooter.GetComponent<Rigidbody> ();
			webParticles [0].gameObject.AddComponent (typeof(SpringJoint));
			AddSpringValues (webParticles [0].gameObject.GetComponent <SpringJoint> ());
		}
	}

	IEnumerator CoAddParticle ()
	{

		//	yield return new WaitForFixedUpdate ();
		yield return new WaitForSeconds (0);


		//	webParticles[webParticles.Count-1].Get

	}

	public void WebDecay ()
	{
		StartCoroutine (CoWebDecay ());
	}

	public IEnumerator CoWebDecay ()
	{
		Destroy (handJoint);
		if (sticking == false)
			webParticles [webParticles.Count - 1].AddForce (transform.up * webExplode, ForceMode.Impulse);
		yield return new WaitForSeconds (5);
		sticking = false;
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).GetComponent <Rigidbody> ().isKinematic = false;
			Destroy (transform.GetChild (i).GetComponent<WebAttacher> ().stickJoint);
			transform.GetChild (i).GetComponent<Rigidbody> ().velocity = Vector3.zero;
			transform.GetChild (i).GetComponent<WebAttacher> ().decaying = true;
		}
		yield return new WaitForSeconds (5);
		Destroy (gameObject);
	}

	public void Stick ()
	{
		sticking = true;

	}

	public void AddSpringValues (SpringJoint particle)
	{
		
		if (webParticles.Count == 2) {
			webParticles [0].gameObject.GetComponent <SpringJoint> ().connectedBody = webParticles [1];
		}
		particle.damper = damper;
		particle.spring = springConstant;
		particle.minDistance = stringLength;
		particle.maxDistance = stringLength;
	}

	public void AddBendSpringValues (SpringJoint particle)
	{
		particle.autoConfigureConnectedAnchor = false;
		particle.anchor = Vector3.zero;
		particle.damper = damper;
		particle.spring = springConstant / 2;
		particle.minDistance = stringLength * 2;
		particle.maxDistance = stringLength * 2;
	}
}
