﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebShooter : MonoBehaviour {

	public Transform attacher;
	public Animator webShooterAnim;
	public List<Vector3> webPos = new List<Vector3> ();
	public List<Vector3> webVel = new List<Vector3> ();
	public List<Vector3> webAcc = new List<Vector3> ();
	public Vector3 gravity;
	public bool sticking, holding;
	public GameObject web;
	public Web currentWeb;

	// Use this for initialization
	void Start () {

		gravity = Physics.gravity;
		sticking = false;
		Physics.IgnoreLayerCollision (8, 8, true);

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			holding = true;
			sticking = false;

			currentWeb = ((GameObject)Instantiate (web, transform.position, transform.rotation)).GetComponent <Web> ();
			currentWeb.webShooter = this.gameObject;
			webShooterAnim.SetTrigger ("shoot");
		}
		if (Input.GetKeyDown (KeyCode.KeypadEnter)) {
			webPos.Clear ();
			webVel.Clear ();
			sticking = false;
		}
		if (Input.GetKey (KeyCode.Space)) {
			if (currentWeb.sticking == false) {
				if ((currentWeb.webPos [currentWeb.webPos.Count - 1] - transform.position).magnitude >= currentWeb.stringLength)
					currentWeb.AddParticle ();

			} else {
				currentWeb.webPos [currentWeb.webPos.Count - 1] = transform.position;
				currentWeb.webVel [currentWeb.webPos.Count - 1] = Vector3.zero;
			}

		} 
		if (Input.GetKeyUp (KeyCode.Space)) {
			if (currentWeb.sticking == false)
				currentWeb.webForces [currentWeb.webPos.Count - 1] += transform.up * currentWeb.webExplode;
			holding = false;
			currentWeb.holding = false;
			webShooterAnim.SetTrigger ("release");
			currentWeb.WebDecay ();
			currentWeb = null;
		}

	
	}


	IEnumerator DebugShoot () {
		yield return new WaitForSeconds (2);
		currentWeb = ((GameObject)Instantiate (web, transform.position, transform.rotation)).GetComponent <Web> ();
		currentWeb.webShooter = this.gameObject;
		webShooterAnim.SetTrigger ("shoot");
	}


}
