﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour {

	public float damper = 0.2f;
	public bool bendingSprings;
	public bool sticking, holding;

	LineRenderer rend;
	public Transform attacher;
	public List<Vector3> webPos = new List<Vector3> ();
	public List<Vector3> webVel = new List<Vector3> ();
	public List<Vector3> webForces = new List<Vector3> ();

	public Vector3 gravity;
	public float tolerance = 0.25f;
	public float webExplode;
	public float springConstant = 1;
	public float stringLength = 1;
	public float particleMass = 0.5f;
	public GameObject webShooter;


	// Use this for initialization
	void Awake () {
		rend = GetComponent <LineRenderer> ();
		rend.numPositions = 1;
		rend.SetPosition (0, transform.position);

		webPos.Add (transform.position + transform.up * webExplode * Time.fixedDeltaTime);
		webVel.Add (Vector3.zero);
		webForces.Add ((transform.up * webExplode) * particleMass);
		webVel [webPos.Count - 1] += webForces [webPos.Count - 1] / particleMass;
		webPos.Add (transform.position);
		webVel.Add (Vector3.zero);
		webForces.Add ((transform.up * webExplode) * particleMass);
		webVel [webPos.Count - 1] += webForces [webPos.Count - 1] / particleMass;
		//Add two points, one that sticks to shooter, one that goes flying
		attacher.GetComponent <WebAttacher> ().web = this;
		gravity = Physics.gravity;
		holding = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		rend.numPositions = webPos.Count;
		for (int i = 0; i < webPos.Count; i++) {
			webForces [i] = Vector3.zero;
			ApplyGravity (i);
			ApplySpringForce (i);
			ApplyForces (i);
			ApplyVelocity (i);
			ApplyPosition (i);
			if (i < 3) {
				Debug.DrawLine (webPos [i], webPos [i] + webForces [i], Color.green);
				Debug.DrawLine (webPos [i], webPos [i] + webVel [i], Color.yellow);
				//	Debug.Log ("id: " + i + ", vel: " + webVel [i]);
			}
		}
		if (attacher != null)
			attacher.position = webPos [0];

	}

	public void WebDecay () {
		StartCoroutine (CoWebDecay ());
	}

	public IEnumerator CoWebDecay () {
		//	Debug.Log ("Starting decay");
		yield return new WaitForSeconds (5);
		//	Debug.Log ("Fall!");
		Destroy (attacher.gameObject);
		sticking = false;
		yield return new WaitForSeconds (5);
		Destroy (gameObject);
	}

	public void Stick () {
		sticking = true;
		webVel [0] = Vector3.zero;
	}

	public void AddParticle () {
		webPos.Add (webShooter.transform.position);
		webVel.Add (Vector3.zero);
		webForces.Add (webShooter.transform.up * webExplode * particleMass);
		webVel [webPos.Count - 1] += webForces [webPos.Count - 1] / particleMass;
		
	}

	void ApplyGravity (int id) {
		if (sticking && id == 0) //If sticking, particle 0 is stuck and is not affected by gravity
			return;
		else
			webForces [id] += gravity * particleMass;
		//webVel [id] += Time.fixedDeltaTime * gravity / particleMass;
	}

	void ApplySpringForce (int id) {
		Vector3 prevSpringForce, nextSpringForce;
		if (sticking == true && id == 0)
			return;
		
		if (id != 0)
			prevSpringForce = (springConstant * ((webPos [id] - webPos [id - 1]).magnitude - stringLength)
			+ (damper * Vector3.Dot (webVel [id - 1] - webVel [id], webPos [id - 1] - webPos [id]) / (webPos [id - 1] - webPos [id]).magnitude)
			) * (webPos [id - 1] - webPos [id]) / (webPos [id] - webPos [id - 1]).magnitude;
		else
			prevSpringForce = Vector3.zero;

		if (id != webPos.Count - 1) {
			nextSpringForce = (springConstant * ((webPos [id + 1] - webPos [id]).magnitude - stringLength)
			+ (damper * Vector3.Dot (webVel [id + 1] - webVel [id], webPos [id + 1] - webPos [id]) / (webPos [id + 1] - webPos [id]).magnitude)	
			) * (webPos [id + 1] - webPos [id]) / (webPos [id + 1] - webPos [id]).magnitude;
			//	Debug.Log ("Nextspringforce: " + nextSpringForce + "id = " + id + ", webPosCount-1 = " + (webPos.Count - 1));
		} else
			nextSpringForce = Vector3.zero;

		if (bendingSprings) {
			Vector3 prevBendForce, nextBendForce;
			if (id < 2) { //no prev
				prevBendForce = Vector3.zero;
			} else {
				prevBendForce = ((springConstant / 2) * ((webPos [id] - webPos [id - 2]).magnitude - stringLength * 2)
				+ (damper * Vector3.Dot (webVel [id - 2] - webVel [id], webPos [id - 2] - webPos [id]) / (webPos [id - 2] - webPos [id]).magnitude)
				) * (webPos [id - 2] - webPos [id]) / (webPos [id] - webPos [id - 2]).magnitude;
			}
				
			if (id > webPos.Count - 3) { //no next
				nextBendForce = Vector3.zero;
			} else {
				nextBendForce = ((springConstant / 2) * ((webPos [id + 2] - webPos [id]).magnitude - stringLength * 2)
				+ (damper * Vector3.Dot (webVel [id + 2] - webVel [id], webPos [id + 2] - webPos [id]) / (webPos [id + 2] - webPos [id]).magnitude)	
				) * (webPos [id + 2] - webPos [id]) / (webPos [id + 2] - webPos [id]).magnitude;
			}
			
			webForces [id] += Time.fixedDeltaTime * prevBendForce + nextBendForce * particleMass;

		}


		webForces [id] += Time.fixedDeltaTime * (prevSpringForce + nextSpringForce) * particleMass;
		 

	}


	void ApplyForces (int id) {
		webVel [id] += webForces [id] * Time.fixedDeltaTime / particleMass;
	}

	void ApplyVelocity (int id) {
		if (tolerance == 0) //Ignorer  maxLength
			webPos [id] += webVel [id] * Time.fixedDeltaTime;
		else {
			if (id < webPos.Count - 1) { //Sidste partikel har frit spil
				webPos [id] += webVel [id] * Time.fixedDeltaTime;
				if (Mathf.Abs ((webPos [id] - webPos [id - 1]).magnitude - stringLength) < tolerance) { //Hvis under maxLength
					//is all good
				} else { //Hvis over maxLength
					//Debug.Log ("Too long!");
					//	Debug.DrawLine (webPos [id], webPos [id] + webVel [id].normalized * maxLength, Color.green);
					webPos [id] = webPos [id - 1] + (webPos [id] - webPos [id - 1]).normalized * (tolerance + stringLength);
				}
			} else
				webPos [id] += webVel [id] * Time.fixedDeltaTime;
			//Debug.Log ("id: " + id + "count-1:" + (webPos.Count - 1));

		}


		if (holding == true && sticking == true && id == webPos.Count - 1) {
			webPos [id] = transform.position;
			//	Debug.Log ("Hold on!");
		}
	}

	void ApplyPosition (int id) {
		
		rend.SetPosition (id, webPos [id]);
	}
}
