﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringTest : MonoBehaviour {
	public List<Transform> particles = new List<Transform> ();
	Vector3 gravity = Physics.gravity;
	public Vector3[] pos, vel, forces;
	public float mass, springConstant, springLength, damper, tolerance;
	bool[] stretched;

	// Use this for initialization
	void Start () {
		pos = new Vector3[particles.Count];
		vel = new Vector3[particles.Count];
		forces = new Vector3[particles.Count];
		stretched = new bool[particles.Count];
		for (int i = 0; i < particles.Count; i++) {
			pos [i] = particles [i].localPosition;
			stretched [i] = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		for (int i = 1; i < particles.Count; i++) {
			forces [i] = Vector3.zero;
			ApplyGravity (i);
			ApplySpringForces (i);
			//if (stretched [i])
			//		ApplyCentripetal (i);

			ApplyForces (i);
			ApplyVelocity (i);
			ApplyPosition (i);

		}
	}


	void ApplyGravity (int id) {
		
		forces [id] += gravity * mass;
	}

	void ApplySpringForces (int id) {
		Vector3 prevSpringForce, nextSpringForce;
		if (id > 0) {
			prevSpringForce = (springConstant * ((pos [id] - pos [id - 1]).magnitude - springLength)//Actual springforce (k * displacement)
			//	+ (damper * Vector3.Dot (vel [id - 1] - vel [id], pos [id - 1] - pos [id]) / (pos [id - 1] - pos [id]).magnitude)           //Damper 
			) * (pos [id - 1] - pos [id]) / (pos [id] - pos [id - 1]).magnitude; //Direction of force normalized
			Debug.DrawLine (particles [id].position, particles [id].position + prevSpringForce, Color.cyan);
		} else
			prevSpringForce = Vector3.zero;

		if (id < pos.Length - 1) {
			nextSpringForce = (springConstant * ((pos [id + 1] - pos [id]).magnitude - springLength)
			//	+ (damper * Vector3.Dot (vel [id + 1] - vel [id], pos [id + 1] - pos [id]) / (pos [id + 1] - pos [id]).magnitude)	
			) * (pos [id + 1] - pos [id]) / (pos [id + 1] - pos [id]).magnitude;
			//	Debug.Log ("Nextspringforce: " + nextSpringForce + "id = " + id + ", webPosCount-1 = " + (webPos.Count - 1));
			//Debug.Log ("Name: " + gameObject.name + ", id: " + id + ", id+1: " + (id + 1));
			Debug.DrawLine (particles [id].position, particles [id].position + nextSpringForce, Color.magenta);
		} else
			nextSpringForce = Vector3.zero;

		Vector3 damping = damper * vel [id];
		forces [id] += (prevSpringForce + nextSpringForce + damping) * mass;
		if (id != 0) {
			//	Debug.DrawLine (particles [id].position, particles [id].position + nextSpringForce + prevSpringForce, Color.white);
		}
	}

	void ApplyCentripetal (int id) {
		Vector3 centripetalForce = (pos [id - 1] - pos [id]).normalized * (mass * vel [id].sqrMagnitude / springLength); //F = mv^2/r
		//Debug.Log ("Centrpetal: " + centripetalForce);
		forces [id] += centripetalForce * Time.fixedDeltaTime / mass;
		Debug.DrawLine (particles [id].position, particles [id].position + centripetalForce, Color.red);
		//Debug.Log ("Centipedes!");
	}

	void ApplyForces (int id) {
		if (id != 0) {
			vel [id] += forces [id] * Time.fixedDeltaTime / mass;
			Debug.DrawLine (particles [id].position, particles [id].position + forces [id], Color.green);
		}
	}

	void ApplyVelocity (int id) {
		if (id > 0) {
			pos [id] += vel [id] * Time.fixedDeltaTime;
			//Debug.DrawLine (particles [id].position, particles [id].position + vel [id], Color.blue);
		}
	}

	void ApplyPosition (int id) {
		
		if (id > 0) {
			if (tolerance != 0) {
				if (Mathf.Abs ((pos [id] - pos [id - 1]).magnitude - springLength) > tolerance) {
					Vector3 prevPos = pos [id];
					pos [id] = pos [id - 1] + (pos [id] - pos [id - 1]).normalized * (springLength + tolerance);
					//Add centripetal force F = mv^2 /r
					stretched [id] = true;
					/*	vel [id] += (mass * vel [id].sqrMagnitude / springLength) * (pos [id] - pos [id - 1]).normalized * Time.fixedDeltaTime / mass;
				Debug.Log ((mass * vel [id].sqrMagnitude / springLength) * (pos [id] - pos [id - 1]).normalized * Time.fixedDeltaTime / mass);
				Debug.Log ("Velsqr = " + vel [id].sqrMagnitude + ", id: " + id);
				Debug.DrawLine (pos [id], pos [id] + (mass * vel [id].sqrMagnitude / springLength) * (pos [id] - pos [id - 1]).normalized * Time.fixedDeltaTime / mass, Color.black);
			*/
					/*Vector3 velChange = pos [id] - prevPos;
				Vector3 prevVel = vel [id];
				vel [id] += velChange;
				Debug.DrawLine (particles [id].position, particles [id].position + velChange * 10, Color.red);*/

				}
			} else {
				stretched [id] = false;
			}
		}
		particles [id].localPosition = pos [id];
//		particles [id].localPosition = new Vector3 (pos [id].x, pos [id].y, id);
	}
}
