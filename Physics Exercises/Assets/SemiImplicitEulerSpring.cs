﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemiImplicitEulerSpring : MonoBehaviour {

	public GameObject spring0;
	Rigidbody rigid;
	public float spaceForce = 2f;
	public float k;
	Vector3 velocity, velocityNextStep, newPosition, positionTilde;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
		velocity = Vector3.zero;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space)) {
			//Debug.Log ("Spacing");
			transform.position += -transform.up * spaceForce * Time.deltaTime;
		}
			

		velocityNextStep = velocity + Time.deltaTime * SpringForce (transform.position) / rigid.mass;
		newPosition = transform.position + Time.deltaTime * velocityNextStep;
		transform.position = newPosition;

		velocity = velocityNextStep;
	}

	Vector3 SpringForce (Vector3 springPosition) {
		return -k * (springPosition - spring0.transform.position);
	}
}
