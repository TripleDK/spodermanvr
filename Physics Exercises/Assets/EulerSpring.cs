﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EulerSpring : MonoBehaviour {

	public GameObject spring0;
	Rigidbody rigid;
	public float spaceForce = 2f;
	public float k;
	Vector3 velocity;
	float energyTotal;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
		velocity = Vector3.zero;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space)) {
			//Debug.Log ("Spacing");
			transform.position += -transform.up * spaceForce * Time.deltaTime;
		}
		Vector3 newPosition = transform.position + Time.deltaTime * velocity;

		velocity = velocity + Time.deltaTime * SpringForce () / rigid.mass;

		transform.position = newPosition;
	}

	Vector3 SpringForce () {
		return -k * (transform.position - spring0.transform.position);
	}
}
