﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingBallInfiniteEnergy : MonoBehaviour {

	Rigidbody rigid;

	public float totalEnergy;
	public float speed;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
		speed = 0;
		totalEnergy = 0.5f * rigid.mass * speed * speed + rigid.mass * (Physics.gravity.magnitude) * transform.position.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		speed = Mathf.Sqrt ((2 / rigid.mass) *
		(totalEnergy - rigid.mass * Physics.gravity.magnitude * transform.position.y));

		if (rigid.velocity.magnitude > 0.0001f)
			rigid.velocity = speed * rigid.velocity / rigid.velocity.magnitude; //new Vector3 (0, (rigid.velocity.y / rigid.velocity.magnitude) * speed, 0);
	}
}
