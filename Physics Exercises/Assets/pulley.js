@script RequireComponent(ConfigurableJoint) 
@script RequireComponent(Rigidbody)
/*@script RequireComponent(LineRenderer)
 #pragma strict

var ropeLength : float;
var otherEnd : GameObject;

var minLimit : float = 0.1;

private var anchorPosition : Vector3;
private var nextLimit : float;


private var joint : ConfigurableJoint;
private var rigid : Rigidbody;
private var otherJoint : ConfigurableJoint;
private var otherPulley : pulley;


function Start () 
{
	anchorPosition = transform.position;
    joint = GetComponent(ConfigurableJoint);
    rigid = GetComponent(Rigidbody);
	nextLimit = joint.linearLimit.limit;
    if (null != otherEnd)
	{
		otherJoint = otherEnd.GetComponent(ConfigurableJoint);
		otherPulley = otherEnd.GetComponent(pulley);
		ropeLength = otherPulley.ropeLength;
	}
	else
	{
		print("Warning: Script pulley requires otherEnd to be set to a GameObject.");
	}
}

function GetActualDistance() : float
{
	return Vector3.Distance(transform.position, anchorPosition);
}

function FixedUpdate () 
{
	rigid.WakeUp();
	  if (null == joint.connectedBody)
	{
		anchorPosition = joint.connectedAnchor;
	}
	else
	{
		anchorPosition = joint.connectedBody.transform.TransformPoint(joint.connectedAnchor);
	}
	 if (null != otherEnd)
	{
		var thisActualDistance : float = GetActualDistance();
		var otherActualDistance : float = otherPulley.GetActualDistance();
		// compute the limits of the springs in the configurable joint for the next frame
		// here we only need to compute the limit for the joint of this rigidbody;
		// the other limit is only computed when needed for the computation.
		// the actual computation of the other limit is performed in a script attached
		// to the other object. 
		if (thisActualDistance > joint.linearLimit.limit && 
			otherActualDistance > otherJoint.linearLimit.limit) 
			// both ends stretched
		{			
			// get forces away from the anchor position
			var thisForce : float = joint.currentForce.magnitude;
	 	 	var otherForce : float = otherJoint.currentForce.magnitude;
			
			// set limits such that these forces are applied on the other end
			nextLimit = Mathf.Max(0.0, 
				thisActualDistance - otherForce / joint.linearLimitSpring.spring);
			var otherNextLimit : float = Mathf.Max(0.0, 
				otherActualDistance - thisForce / otherJoint.linearLimitSpring.spring);	
			
			// scale limits to ensure that they match ropeLength	
			if (nextLimit + otherNextLimit > ropeLength)
			{
				nextLimit = nextLimit * ropeLength / (nextLimit + otherNextLimit);
				otherNextLimit = otherNextLimit * ropeLength / (nextLimit + otherNextLimit);
			}

			// make sure the limits are never smaller than minLimit
			if (nextLimit < minLimit) 
			{
				nextLimit = minLimit;
				// otherNextLimit = ropeLength - minLimit;
			}
			if (otherNextLimit < minLimit)
			{
				// otherNextLimit = minLimit;
				nextLimit = Mathf.Max(minLimit, ropeLength - minLimit);
			}
		}
		else if (thisActualDistance <= joint.linearLimit.limit && 
			otherActualDistance > otherJoint.linearLimit.limit) 
			// only other end is stretched
		{
			// set this limit such that we are still not stretched
			nextLimit = Mathf.Max(minLimit, thisActualDistance);
		}
		else if (thisActualDistance > joint.linearLimit.limit && 
			otherActualDistance <= otherJoint.linearLimit.limit) 
			// only this end is stretched
		{
			// set this limit to the maximum allowed by the ropeLength 
			// without putting any force on the other end
			nextLimit = Mathf.Max(minLimit, ropeLength - otherActualDistance);
		}
		else // none of the ends is stretched 
		{
			// distribute the additional rope length between both ends
			nextLimit = Mathf.Max(minLimit, thisActualDistance + 
				0.5 * (ropeLength - thisA c t u a l D i s t a n c e   -   o t h e r A c t u a l D i s t a n c e ) ) ; 
 	 	 } 
 	 	 
 	 	 / /   u p d a t e   l i m i t s 
 	 	 i f   ( o t h e r P u l l e y . n e x t L i m i t   ! =   o t h e r J o i n t . l i n e a r L i m i t . l i m i t ) 	 
 	 	 	 / /   h a s   t h e   o t h e r   e n d   b e e n   u p d a t e d   a l r e a d y ? 
 	 	 	 / /   ( O t h e r w i s e   w e   d o   n o t h i n g   h e r e   a n d   
 	 	 	 / /   u p d a t e   t h e   l i m i t s   i n   t h e   F i x e d U p d a t e   f u n c t i o n   o f   t h e   o t h e r E n d . )   
 	 	 { 
 	 	 	 / /   t h e n   i t   i s   t i m e   t o   u p d a t e   t h e   l i m i t s   
 	 	 	 j o i n t . l i n e a r L i m i t . l i m i t   =   n e x t L i m i t ; 
 	 	 	 o t h e r J o i n t . l i n e a r L i m i t . l i m i t   =   o t h e r P u l l e y . n e x t L i m i t ; 
 	 	 } 
 	 } 
 } 
 
 / /   f u n c t i o n   f o r   r e n d e r i n g   r o p e s 
 f u n c t i o n   U p d a t e ( ) 
 { 
 	 v a r   l i n e s   :   L i n e R e n d e r e r   =   G e t C o m p o n e n t ( L i n e R e n d e r e r ) ; 
 
 	 i f   ( n u l l   ! =   l i n e s ) 
 	 { 
 	 	 l i n e s . S e t P o s i t i o n ( 0 ,   a n c h o r P o s i t i o n ) ; 
 	 	 l i n e s . S e t P o s i t i o n ( 1 ,   t r a n s f o r m . p o s i t i o n ) ; 
 	 } 
 } 
 */