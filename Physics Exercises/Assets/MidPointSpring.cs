﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPointSpring : MonoBehaviour {

	public GameObject spring0;
	Rigidbody rigid;
	public float spaceForce = 2f;
	public float k;
	Vector3 velocity, velocityHalfStep;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space)) {
			//Debug.Log ("Spacing");
			transform.position += -transform.up * spaceForce * Time.deltaTime;
		}
		Vector3 halfStepPosition = transform.position + Time.deltaTime * velocity / 2;
		Vector3 newPosition = transform.position + Time.deltaTime * velocityHalfStep;

		velocityHalfStep = velocity + Time.deltaTime * SpringForce (transform.position) / (rigid.mass * 2);
		velocity = velocity + Time.deltaTime * SpringForce (halfStepPosition) / rigid.mass;



		transform.position = newPosition;
	}

	Vector3 SpringForce (Vector3 springPosition) {
		return -k * (springPosition - spring0.transform.position);
	}
}
