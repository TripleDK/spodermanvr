﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRigibody : MonoBehaviour {
	public Quaternion rotation;
	public Vector3 angulVel;
	public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		speed = angulVel.magnitude;
		transform.RotateAround (transform.position, angulVel, speed);
	}
}
