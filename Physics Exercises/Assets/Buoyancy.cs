﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buoyancy : MonoBehaviour {

	Rigidbody rigid;
	public 	float volume;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
		volume = 4f / 3f * Mathf.PI;
		Debug.Log ("Volume is " + volume);
		volume *= Mathf.Pow (transform.localScale.y * 0.5f, 3);
		Debug.Log ("Volume is " + volume);
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	void OnTriggerStay (Collider col) {
		//Debug.Log ("Colliding");
		if (col.gameObject.name == "Water") {
			float dispFluid = 0;
			
			Vector3 sphereCenter = transform.position;
			float radius = transform.localScale.y * 0.5f;
			float sphereTop = sphereCenter.y + radius;
			float sphereBot = sphereCenter.y - radius;
			float waterSurface = col.bounds.max.y;
			float h = 0;


			if (sphereCenter.y > waterSurface) { //Center above surface, possible bottom submerged
				h = waterSurface - sphereBot;
			}
			if (sphereCenter.y < waterSurface) { //Center below surface, buttom submerged, possible top submerged
				h = sphereTop - waterSurface;
			}
			if (h > radius)
				h = radius;
			if (h < 0)
				h = 0;


			float asquared = (2f * radius * h - h * h);
			float a = Mathf.Sqrt (Mathf.Abs (asquared));


			if (sphereCenter.y > waterSurface) { //Center above surface, possible bottom submerged
				dispFluid = (Mathf.PI * h / 6f) * (3f * a * a + h * h);
				Debug.DrawLine (new Vector3 (0, sphereBot + h, 0), new Vector3 (0, sphereBot + h, a), Color.yellow);
			}
			if (sphereCenter.y < waterSurface) { //Center below surface, buttom submerged, possible top submerged
				dispFluid = volume;
				dispFluid -= (Mathf.PI * h / 6f) * (3f * a * a + h * h);
			}


			/*	if (sphereCenter.y < waterSurface) { //Center below - very submerged
				Debug.DrawLine (transform.position + new Vector3 (0, sphereBot + h, 0), transform.position + new Vector3 (0, sphereBot + h, a), Color.yellow);
				dispFluid = volume;
				dispFluid -= (Mathf.PI * h / 6) * 3 * a * a + h * h;
				//dispFluid += 0.5f * (4 / 3) * Mathf.PI * Mathf.Pow (radius, 3);
			} else { //Center above - slightly submerged
				Debug.DrawLine (transform.position + new Vector3 (0, sphereTop - h, 0), transform.position + new Vector3 (0, sphereTop - h, a), Color.yellow);
				dispFluid = (Mathf.PI * h / 6) * 3 * a * a + h * h;

			}*/
			Debug.Log ("Displaced fluid = " + dispFluid + ", h = " + h + ", a squared = " + asquared + ", radius = " + radius + ", extra dispFluid is " + (Mathf.PI * h / 6) * 3 * a * a + h * h);
			rigid.AddForce (Physics.gravity.magnitude * (dispFluid / 1000) * Vector3.up, ForceMode.Force);
			Debug.DrawLine (transform.position, transform.position + Physics.gravity.magnitude * dispFluid * Vector3.up, Color.green);
		}
	}

}
