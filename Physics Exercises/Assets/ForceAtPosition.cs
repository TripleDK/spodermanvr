﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceAtPosition : MonoBehaviour {

	public Vector3 localForceCor;
	public float forceStrength;
	Rigidbody rigid;

	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigid.AddForceAtPosition (-Physics.gravity.normalized * forceStrength, transform.TransformPoint (localForceCor));
		Debug.DrawLine (transform.position, transform.position + Physics.gravity, Color.red);
		Debug.DrawLine (transform.TransformPoint (localForceCor), transform.TransformPoint (localForceCor) - Physics.gravity.normalized * forceStrength, Color.green);
	}
}
