﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateQuaternion : MonoBehaviour {

	public Vector3 angulVel;
	private Quaternion w2, qdot, qnew;
 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		w2 = new Quaternion (angulVel.x / 2, angulVel.y / 2, angulVel.z / 2, 0);
		qdot = w2 * transform.rotation;
		qnew = new Quaternion (
			transform.rotation.x + qdot.x * Time.deltaTime,
			transform.rotation.y + qdot.y * Time.deltaTime,
			transform.rotation.z + qdot.z * Time.deltaTime,
			transform.rotation.w + qdot.w * Time.deltaTime
		);
		transform.rotation = qnew;
	}
}
