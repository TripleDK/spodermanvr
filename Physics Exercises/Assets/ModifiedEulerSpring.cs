﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifiedEulerSpring : MonoBehaviour {

	public GameObject spring0;
	Rigidbody rigid;
	public float spaceForce = 2f;
	public float k;
	Vector3 velocity, velocityTilde, newPosition, positionTilde;

	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space)) {
			//Debug.Log ("Spacing");
			transform.position += -transform.up * spaceForce * Time.deltaTime;
		}
		positionTilde = transform.position + Time.deltaTime * velocity;

		velocityTilde = velocity + Time.deltaTime * SpringForce (transform.position) / rigid.mass;

		newPosition = transform.position + Time.deltaTime * (velocity + velocityTilde) / 2;

		velocity = velocity + Time.deltaTime * (SpringForce (transform.position) + SpringForce (positionTilde)) / (rigid.mass * 2);

		transform.position = newPosition;
	}

	Vector3 SpringForce (Vector3 springPosition) {
		return -k * (springPosition - spring0.transform.position);
	}
}
