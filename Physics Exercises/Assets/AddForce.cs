﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour {

	public	Vector3 velocity;
	public bool force, acc, velChange, impulse;
	public Vector3 acceleration;
	public	float mass;
	public Vector3 gForce;
	Rigidbody rigid;

	void FixedUpdate () {
		//	transform.position = transform.position + velocity * Time.fixedDeltaTime;

		//	velocity = velocity + acceleration * Time.fixedDeltaTime;
		gForce = -9.81f * rigid.mass * Vector3.up;
		//	acceleration = gForce * Mathf.Pow (mass, -1);

		if (force)
			rigid.AddForce (gForce, ForceMode.Force);
		if (acc)
			rigid.AddForce (gForce / rigid.mass, ForceMode.Acceleration);
		if (velChange)
			rigid.AddForce ((gForce * Time.fixedDeltaTime / rigid.mass), ForceMode.VelocityChange);
		if (impulse)
			rigid.AddForce (gForce * Time.fixedDeltaTime, ForceMode.Impulse);
	
	
	}


	// Use this for initialization
	void Start () {
		rigid = GetComponent <Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
